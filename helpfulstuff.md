---
layout: default
permalink: helpfullstuff
---

# Helpful stuff

## Swiss-knife OS

- [Kali Linux](https://www.kali.org) представляет из себя дистрибутив, содержащий множество утилит для проведения тестирования на проникновение — от анализа уязвимостей веб-приложений, до взлома сетей и сервисов и закрепления в системе.
 
## Git

- [Git Magic](http://www-cs-students.stanford.edu/~blynn/gitmagic/intl/en/) - это лучший, по моему мнению, учебние по контролю версий git 
- [GitLab](https://gitlab.com) - бесплатный и хороший сервис контроля версий с pages, pipeline, CI, wiki и прочими ништяками


<section>

  <h2>Наши проекты</h2>

  <div class="grid t-hackcss-cards">

    {% for project in site.projects %}

    <div class="cell -6of12 t-hackcss-cards-cell">
      <div class="card">
        <header class="card-header">{{ project.name }}</header>
        <div class="card-content">
          <div class="inner">

            {% if project.image %}
            <img src="{{ project.image }}" class="t-hackcss-cards-image"
                 alt="{{ project.name }}"
                 title="{{ project.name }}" />
            {% endif %}

            <p class="t-hackcss-cards-text">{{ project.description }}</p>
            <p class="t-hackcss-cards-link">
              <a href="{{ project.link }}" title="{{ project.name }}" target="_blank">
                 {{ project.name }}
               </a>
            </p>
          </div>
        </div>
      </div>
    </div>

    {% endfor %}

  </div>
</section>