---
layout: default
permalink: meetup
---

<section>
  <h1>Встречи</h1>

  <p>
    На этой странице будет информация о наших встречах, полезная информация о проектах, которые мы делаем. Так же здесь будут видео с выступлений :)
  </p>
</section>

<section>
  <h2>Посты, встречи</h2>

  {% for post in site.posts %}
    {% assign nth = forloop.index0 | modulo:2 %}
    <div class="media t-hackcss-media">

      {% if nth == 0 %}
      <div class="t-hackcss-media-shift media-left">
        <img class="t-hackcss-media-image"
             src="{{ post.image_preview }}" alt="{{ post.title }}"
             title="{{ post.title }}" />
      </div>
      {% endif %}

      <div class="media-body">
        <div class="media-heading">
          <span>{{ post.date | date_to_string }} &raquo;
            {% if post.external_url %}
              <a href="{{ post.external_url }}" target="_blank" title="{{ project.name }}">
                {{ post.title }}
              </a>
            {% else %}
              <a href="{{ post.url | prepend: site.baseurl }}" title="{{ project.name }}">
                {{ post.title }}
              </a>
            {% endif %}
          </span>
        </div>
        <div class="media-content">
          {{ post.short_description }}
        </div>
      </div>

      {% if nth == 1 %}
      <div class="t-hackcss-media-shift media-right">
        <img class="t-hackcss-media-image"
             src="{{ post.image_preview }}" alt="{{ post.title }}"
             title="{{ post.title }}" />
      </div>
      {% endif %}
    </div>
  {% endfor %}
</section>

<section>
  <h2>test</h2>

  {% for post in site.posts %}
    {% assign nth = forloop.index0 | modulo:2 %}
    <div class="media t-hackcss-media">

      {% if nth == 0 %}
      <div class="t-hackcss-media-shift media-left">
        <img class="t-hackcss-media-image"
             src="{{ post.image_preview }}" alt="{{ post.title }}"
             title="{{ post.title }}" />
      </div>
      {% endif %}

      <div class="media-body">
        <div class="media-heading">
          <span>{{ post.date | date_to_string }} &raquo;
            {% if post.external_url %}
              <a href="{{ post.external_url }}" target="_blank" title="{{ project.name }}">
                {{ post.title }}
              </a>
            {% else %}
              <a href="{{ post.url | prepend: site.baseurl }}" title="{{ project.name }}">
                {{ post.title }}
              </a>
            {% endif %}
          </span>
        </div>
        <div class="media-content">
          {{ post.short_description }}
        </div>
      </div>

      {% if nth == 1 %}
      <div class="t-hackcss-media-shift media-right">
        <img class="t-hackcss-media-image"
             src="{{ post.image_preview }}" alt="{{ post.title }}"
             title="{{ post.title }}" />
      </div>
      {% endif %}
    </div>
  {% endfor %}
</section>
